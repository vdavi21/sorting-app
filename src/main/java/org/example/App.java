package org.example;
import java.util.Arrays;

public class App
{
    public static String answer;

    public static void main( String[] args ) {

        if (args.length == 0) {
            System.out.println("No arguments.");
            answer = "No arguments.";
            return;
        }
        int n = args.length;
        int[] numbers = new int[n];

        for (int i = 0; i < n; i++) {
            numbers[i] = Integer.parseInt(args[i]);
        }
        Arrays.sort(numbers);
        answer =Arrays.toString(numbers);
        System.out.println(Arrays.toString(numbers));
    }
    public static String getAnswer(){
        return answer;
    }
}


