package org.example;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * Unit test for simple App.
 */
@RunWith(Parameterized.class)
public class MainTest {
    private final String[] args;
    private final String expected;


    /**
     * Create the test case
     *
     * @param args name of the test case
     */

    public MainTest(String[] args, String expected){
        this.args = args;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{}, "No arguments."},
                {new String[]{"1"}, "[1]"},
                {new String[]{"42", "56", "89", "7", "23", "99", "11", "5", "456", "789"},
                        "[5, 7, 11, 23, 42, 56, 89, 99, 456, 789]"},
                {new String[]{"36", "82", "14", "57", "91", "23", "5", "68", "42", "19", "73", "8", "60", "99", "31"},
                        "[5, 8, 14, 19, 23, 31, 36, 42, 57, 60, 68, 73, 82, 91, 99]"}
        });
    }

    @Test
    public void testSorting(){

        Main.main(args);

        assertEquals(expected, Main.getSortedInput());
    }

}
